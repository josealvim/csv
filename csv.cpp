#include <csv/csv.hpp>

#include <algorithm>
#include <iostream>

#include <cassert>

namespace csv {

class impl_reader_t {	
	std::istream& stream;
	std::string field;
	record_t record;

	char curr;
	bool in_string;
	bool was_in_string;

	bool LF() {
		return curr == '\n';
	}

	bool CRLF() {
		return curr == '\r' and stream.peek() == '\n';
	}

	bool end_of_record() {
		return 	stream.eof() 
			or (not in_string and LF()) 
			or (not in_string and CRLF());
	}

	bool end_of_field() {
		return 	end_of_record() 
			or (not in_string and curr == ',');
	}

	void add_character (char c) {
		if (in_string or not was_in_string)
			field += c;
		else {
			// warn user
		}
	}

	void init_string() {
		if (field.size() != 0) {
			// warn_user
		}

		in_string = true;
		was_in_string = true;
		field.clear();	
	}

	void fini_string() {
		in_string = false;
	};

	void commit_field() {
		record.push_back(field);
		field.clear();
		was_in_string = false;
	}

	public:
	impl_reader_t(std::istream&s): stream(s) {
		curr          = false;
		in_string     = false;
		was_in_string = false;
	}

	record_t operator()() {
		if (!stream or stream.peek() <= 0)
			return {}; 

		while (!end_of_record()) {
			stream.get(curr);
			if (curr == '"') {
				if (not in_string) {
					init_string();
					continue;
				} else
				if (stream.peek() == '"') {
					stream.get(curr);
				} 
				else { 
					fini_string();
				}
			}
			if (end_of_field()) {
				// don't forget to get rid of \r
				if (CRLF()) stream.get(curr);
				commit_field();
			} else {
				add_character(curr);
			}
		}

		was_in_string = false;
		if (in_string) {
			// warn user
		}
		
		record_t result {std::move(record)};
		record.clear();
		return result;
	}
};

record_t read_record(std::istream& stream) {
	return impl_reader_t(stream)();
}

} // csv

#pragma once

#include <istream>
#include <string>
#include <vector>
#include <map>

namespace csv {

using field_t  = std::string;
using record_t = std::vector<field_t>;
using mapped_t = std::map<std::string, field_t>;

record_t read_record(std::istream& from);

class iterator_sentinel {};

class record_iterator {
	std::istream& stream;

	public: 
	record_iterator(std::istream& from):
		stream(from) {}

	record_iterator operator++() const {
		return record_iterator(stream);
	}

	record_t operator*() const {
		return read_record(stream);
	}

	bool operator==(iterator_sentinel) {
		return stream.eof() or stream.peek() <= 0;
	}
};

class record_reader_t {
	std::istream& from;

	public:
	record_reader_t(std::istream& from): 
		from(from) {};

	record_iterator begin() {
		return record_iterator(from);
	}

	iterator_sentinel end() {
		return iterator_sentinel {};
	}
};

class mapped_iterator {
	std::istream& stream;
	record_t const& header;

	public:
	struct SizeMismatchError {};

	mapped_iterator(std::istream& from, record_t const& header):
		stream(from), header(header) {}
	
	mapped_t operator*() const {
		auto curr = read_record(stream);
		mapped_t result;
		if (curr.size() != header.size())
			throw SizeMismatchError {};

		for (size_t i = 0; i < curr.size(); i++)
			result[header[i]] = curr[i];
		return result;
	}

	mapped_iterator operator++() const {
		return mapped_iterator(stream, header);
	}

	bool operator==(iterator_sentinel) {
		return stream.eof() or stream.peek() <= 0;
	}
};

class mapped_reader_t {
	std::istream& stream;
	record_t _header;

	public:
	record_t const& header;

	mapped_reader_t(std::istream& stream):
		stream(stream), 
		_header(read_record(stream)),
       		header(_header)	{}

	mapped_iterator begin() {
		return mapped_iterator(stream, header);
	}

	iterator_sentinel end() {
		return iterator_sentinel {};
	}
};

}
